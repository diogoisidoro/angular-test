import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImageListComponent } from './image-list/image-list.component';
import { ManageInfoComponent } from './manage-info/manage-info.component';


const routes: Routes = [
  { path: 'images', component: ImageListComponent },
  { path: '', component: ManageInfoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
